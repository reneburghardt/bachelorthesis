\documentclass[
	a4paper,
	pagesize,
	pdftex,
	12pt,
	twoside, % + BCOR darunter: für doppelseitigen Druck aktivieren, sonst beide deaktivieren
	BCOR=5mm, % Dicke der Bindung berücksichtigen (Copyshop fragen, wie viel das ist)
	ngerman,
	fleqn,
	final,
	]{scrartcl}
\usepackage{ucs}
\usepackage[utf8x]{inputenc} % Eingabekodierung: UTF-8
\usepackage[T1]{fontenc} % ordentliche Trennung
\usepackage[ngerman]{babel}
\usepackage{lmodern} % ordentliche Schriften
\usepackage[unicode=true]{hyperref}
\usepackage{setspace,graphicx,tikz,tabularx} % für Elemente der Titelseite
\usepackage[draft=false,babel,tracking=true,kerning=true,spacing=true]{microtype}
 % optischer Randausgleich etc.
 
\usepackage{caption}
\usepackage{multirow}
\usepackage{url}
\usepackage{subfiles}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{makecell}
\usepackage{color}
\usepackage{colortbl}
\usepackage[ruled,linesnumbered]{algorithm2e}
\SetKwInput{KwInput}{Input}
\SetKw{In}{in}
\SetKw{Raise}{raise}
\renewcommand\cellalign{tl}
\renewcommand\theadfont{\bfseries}
\makeatletter
\newcommand\cellwidth{\TX@col@width}
\makeatother
\graphicspath{{images/}{./images/}}
\setlength\columnsep{1cm}
\setcounter{secnumdepth}{3} % sections are level 1
\setcounter{tocdepth}{2}
\addto\captionsngerman{\renewcommand{\refname}{Referenzen}}
\newcommand{\SMALL}{\fontsize{9}{9}\selectfont}
\newcommand\bref[1]{[\ref{#1}]}
\newcommand*{\tabref}[1]{\hyperref[{#1}]{[Tab. \ref{#1}]}}
\newcommand*{\figref}[1]{\hyperref[{#1}]{[Abb. \ref{#1}]}}
\newcommand*{\algref}[1]{\hyperref[{#1}]{[Zeile \ref{#1}]}}
\newcommand*{\fullref}[1]{\hyperref[{#1}]{\nameref*{#1} [\ref{#1}]}}
\newcolumntype{s}{>{\hsize=.15\hsize}r}
\usepackage[toc]{appendix}

\definecolor{gr01}{RGB}{68,156,80}
\definecolor{gr02}{RGB}{139,207,137}
\definecolor{gr03}{RGB}{203,235,197}
\definecolor{gr04}{RGB}{229,245,224}
\definecolor{re01}{RGB}{163,15,21}
\definecolor{re02}{RGB}{204,25,30}
\definecolor{re03}{RGB}{247,92,65}
\definecolor{re04}{RGB}{253,202,181}

\begin{document}

% Beispielhafte Nutzung der Vorlage für die Titelseite (bitte anpassen):
\input{Institutsvorlage}
\titel{Aufwandsschätzungen von Softwareprojekten durch Random Forest} % Titel der Arbeit
\typ{Bachelorarbeit} % Typ der Arbeit:  Diplomarbeit, Masterarbeit, Bachelorarbeit
\grad{Bachelor of Science (B. Sc.)} % erreichter Akademischer Grad
% z.B.: Master of Science (M. Sc.), Master of Education (M. Ed.), Bachelor of Science (B. Sc.), Bachelor of Arts (B. A.), Diplominformatikerin
\autor{René Burghardt} % Autor der Arbeit, mit Vor- und Nachname
\gebdatum{12.10.1994} % Geburtsdatum des Autors
\gebort{Berlin} % Geburtsort des Autors
\gutachter{Prof. Dr. Ulf Leser}{Prof. Dr. Timo Kehrer} % Erst- und Zweitgutachter der Arbeit
\mitverteidigung % entfernen, falls keine Verteidigung erfolgt
\makeTitel

% Hier folgt die eigentliche Arbeit (bei doppelseitigem Druck auf einem neuen Blatt):

\subfile{sections/abstract}

\cleardoublepage

\tableofcontents

\cleardoublepage
 
%\twocolumn
 
\subfile{sections/introduction}

\cleardoublepage
 
\subfile{sections/background}

\cleardoublepage
 
\subfile{sections/related-work}

\cleardoublepage
 
\subfile{sections/experimental-design}

\cleardoublepage
 
\subfile{sections/results}

\cleardoublepage
 
\subfile{sections/conclusion}

\cleardoublepage

\appendix
\subfile{sections/appendix}

\cleardoublepage

%\onecolumn

\bibliographystyle{alpha}
\bibliography{references}

% Erzeugen der Selbständigkeitserklärung auf einem neuen Blatt:
\selbstaendigkeitserklaerung{\today}

\end{document}