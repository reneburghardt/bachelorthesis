-was?
-vorteile: categorical features for building model without converting
-data-dependent, viel configuration
-first Quinlan...


-------------------------------------

The main concern of this paper will focus on MT. MT [23, 25] is
a special type of decision tree and regression tree, but unlike
regression tree that have numerical values at the leaves, the MT
have linear functions as illustrated in Figure 1. MT is one of the
powerful methods for performing regression since it can include
categorical features in constructing such model without the need
to convert them into dummy variables as performed in the basic
regression models. But, like other machine learning techniques,
the performance of MT is a data dependent and has large space of
configuration possibilities and design options induced for each
individual dataset. So it is not surprise to see contradictory results
and different performance figures when make slight changes to
MT parameters. Such parameters include selection of minimum
number of cases (C ) that one node may represent, Whether to
prune the tree (P ), finding smoothing coefficient (K ), and split
threshold(T ).

However, Software effort estimation is a regression problem [17]
and the machine learning based regression are appropriate for this
kind of problem. In this paper we focus on MT. Although quite
emphasis was placed on neural network based regression [ ],
support vector regression [1] and stepwise regression [14] there is
quite small research papers investigated the performance of MT in
software effort estimation such as [1, 17]. These studies reported
that the choice of parameters values have strong impact on the
accuracy of MT where inappropriate setting can lead to
over/under fitting, that is, bad estimation. In other words, the
relative performance of MT depends on the size and the
characteristics of the dataset. This finding has motivated the
current research work.
The MT was first proposed by Quinlan [23, 25] and is considered
a special type of decision tree model developed for the task of
regression. However, the main difference between MT and basic
regression trees is that the leaves of regression trees present
numerical values only, whereas the leaves of a MT have linear
functions as illustrated in Figure 1. The general model tree
building methodology allows input variables to be a mixture of
continuous and categorical variables which is considered useful
for software datasets that have complex structure. The final MT
consists of a tree with linear regression functions at the leaves,
and the prediction for an instance is obtained by sorting it down to
a leaf and using the prediction of the linear model associated with
that leaf. The principle behind MTs is fairly simple, that is, it is
constructed through a process known as binary recursive

partitioning method. This is an iterative process of splitting the
data into partitions, and then splitting it up further on each of the
branches [25]. A tree is formed of decision nodes where a test is
made against data for a given instance, and a choice must be made
about which of the node‘s children is traversed next. The choice
of each node of the tree is usually guided by a least squares error
criterion. The real strength of MTs, however, lies in their inherent
simplicity, and the ease with which they can be interpreted by
non-experts in either computing or the particular application
subject.
In this paper the M5P algorithm [23, 25] has been adopted to
develop MT based software effort estimation. M5P is a powerful
implementation of Quinlan's M5 algorithm for inducing both
Model Trees and Regression Trees [23]. Each training sample has
a set of attributes that can be either numeric or categorical. The
objective of M5P is to construct a model using the training set
that is able to predict continuous dependent variable. The
procedure of Model tree construction using M5P algorithm
consists of three main steps [25]:
1. Tree construction: In the first phase, a standard
regression tree is grown whereby the decision-tree
induction algorithm is used to construct MT by
minimizing the intra-subset variation in the class values
down each branch in order to split the tree. The  ̳purity‘
measure for the splitting criterion is the standard
deviation of the class values of the examples at a node:
the algorithm selects the attribute to split on as the one
giving the largest decrease in standard deviation. The
splitting procedure can stop when only few instances
remain or when the class values of all instances that
reach a node vary very slightly. After the initial tree is
grown, a linear regression model is built for every node
in the tree.
2. Pruning: The constructed tree from step 1 is then pruned
back from each leaf such that each inner node becomes
a leaf node with a regression plane. The main objective
of pruning is to find an estimate of the  ̳true error‘ for
the subtree and the regression function at every node of
the tree. The true error is the expected error of the
subtree/regression on unseen instances that are sorted
down to that node in the tree. The outcome of this stage
is the pruned tree.
3. Smoothing: a smoothing procedure is performed to
avoid sharp discontinuities between adjacent linear
models at the leaves of the pruned tree (i.e. the target
value could change considerably if the value for an
attribute varies a bit so that it is sorted into a different
leaf). This procedure combines the leaf model
prediction with each node along the path back to the
root, smoothing it at each of these nodes by combining
it with the value predicted by the linear model for that
node.
In order to construct optimized MT (OMT), several
parameters must be initially set. These parameters vary from
dataset to another depending on the characteristics of that
dataset. Practically, it is very hard to discover appropriate
values for such parameters unless an optimization algorithm
is applied. However, these parameters are:

 The minimum number of training data cases (C) one
node may represent.
Prune (P): Whether to prune the tree.
 Smoothing coefficient (K) for the smoothing process.
For larger values, more smoothing is applied. For large
(relatively to the number of training data cases) values,
the tree will essentially behave like containing just one
leaf (corresponding to the root node). For value 0, no
smoothing is applied.
 Splitting Threshold (T): Where the splitting procedure
should stop. A node is not splitted if the standard
deviation of the output variable values at the node is
less than Splitting Threshold of the standard deviation
of the output variable values of the entire original data
set.