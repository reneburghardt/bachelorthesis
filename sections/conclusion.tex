\documentclass[../main.tex]{subfiles}
 
\begin{document}
 
\section{Fazit} \label{conclusion}

An dieser Stelle soll ein Fazit über die in dieser Arbeit hervorgebrachten Erkenntnisse gezogen werden. Dabei soll in erster Linie das Erreichen des Ziels aus \fullref{introduction} infrage gestellt und beantwortet werden: Die Evaluation von Random Forest mit Model Trees und Vergleich zu Decision Trees, Model Trees bzw. Random Forest mit Decision Trees. Zusätzlich sollen erweiterte Erkenntnisse aus der Forschung hier Platz finden. Darüber hinaus soll die Validität der hier gemachten Aussagen betrachtet sowie ein Ausblick in die Zukunft mit Vorschlägen für weitere Forschungsgegenstände gemacht werden.

Die \fullref{results} zeigen im Allgemeinen zuerst, dass Tree- bzw. Forest-basierende Methoden in der Praxis noch nicht einsatzfähig sind. Der als akzeptabel angesehene Wert von 75\% für PRED(0,25) \bref{metriken} wird auf keinem Datensatz und mithilfe keiner Methode ansatzweise erreicht. Der erreichte Höchstwert liegt mit \textasciitilde 46\% deutlich drunter. Dass die hier generierten Ergebnisse für die Model Tree-basierenden Methoden nicht an die Erwartungen heranreichen konnten, liegt möglicherweise an folgenden Ursachen:

\begin{itemize}
\item Die Bemühungen, Zufallsfaktoren soweit wie möglich bei der Generierung der Ergebnisse auszuschließen (beispielsweise durch 30-fache Ausführung um nicht abhängig zu sein vom zufälligen Split der Datensätze), haben eventuell zu realistischeren Werten geführt.
\item Die Nutzung der normalen Split-Kriterien für Decision Trees beim Generieren der Model Trees ist inadäquat und provoziert schlechte Zahlen. Dies sollte in Zukunft überprüft werden \bref{future-work}.
\end{itemize}

Darüber hinaus war zu beobachten, dass RFDT über alle genutzten Datensätze hinweg die besten Werte produzierte. Auf dem zweiten Platz folgt RFMT, womit Random Forest-basierende Methoden vorteilhaft gegenüber tree-basierende Methoden sind. Dies deckt sich mit \cite{Nassif2013,Zakrani2018} Zwischen DT und MT gab es keinen nennenswerten Unterschied.

Auch die Zuverlässigkeit der Methoden ist noch mit zu viel Unsicherheit behaftet \bref{reliability}. Aber sowohl bei der Präzision als auch bei der Zuverlässigkeit ist zu beobachten, dass die Ergebnisse stark abhängig von den Daten sind. Dies bestätigt \cite{Srinivasan1995,Radlinski10onpredicting}.

\subsection{Validität}

Im Folgenden soll die Validität der getroffenen Aussagen anhand der gesammelten Erfahrungen und Ergebnisse besprochen und selbstkritisch betrachtet werden.

Obwohl die Vorauswahl der möglichen Parameter für die Methoden in \fullref{hypertuning} erklärt wird, folgt sie keinem reproduzierbarem und beweisbarem Schema. So ist es durchaus im Bereich des Möglichen, dass wichtige Parameter bzw. Parameterkombinationen schon im Vorhinein ausgeschlossen wurden und die wirklichen Ergebnisse negativ verschoben wurden.

Auch kann die festgelegte Anzahl an 20 Bäumen pro Random Forest während der Parameteroptimierung gegenüber den 2000 Bäumen während der Evaluationsphase Probleme bereiten: Die beste Parameterkombination für 20 Bäume muss nicht zwingend die beste für 2000 Bäume sein, jedoch musste die Ausführungszeit reduziert werden.

Ähnliches gilt für die festgelegten Anzahl von 100 Versuchen, die beste Parameterkombination zufällig zu finden. Es ist nicht formell bewiesen, dass 100 Iterationen bei der Suche ausreichend sind. Gewählt wurde die Zahl eher aus zeitlichen Gründen. Wenn möglich sollte in Zukunft jedoch eine umfassendere Suche angewendet werden.

Dadurch, dass auf das nachträgliche Pruning der Bäume zugunsten von Parameter während der Generierung verzichten worden ist, ist Overfitting eine durchaus reale Gefahr. Dazu kommt, dass die Parameter während der Optimierung auch anhand PRED(0,25), also einer Präzisionsmetrik, gewählt worden waren.

Ein weiterer Punkt ist, dass auf Feature Selection verzichtet worden war. So ist es möglich, dass absolut unbedeutende Attribute in den Modellen wichtigen Einfluss bekommen haben.

Laut \cite{Kocaguneli2013} ist Leave-One-Out-Kreuzvalidierung das Mittel der Wahl bei kleinen Datensätzen, wohingegen hier uneingeschränkt ein 30-facher zufälliger Split eingesetzt worden war.

Die relativ geringe Größe der Datensätze ist eine weitere Gefahr im Allgemeinen für den Einsatz von Modellen des maschinellen Lernens. Andererseits würden in der Praxis der Großteil der Firmen bei der Anwendung der Methoden eher selten ähnlich große, geschweige denn größere Datensätze zur Verfügung stehen. Zudem sind die Datensätze alle relativ alt und stellen kaum die heutige Wirklichkeit in der Softwareentwicklung dar.

Zuletzt wurden auch Attribute beim Erlernen der Modelle nicht ausgeschlossen, die in der Praxis noch gar nicht zum Zeitpunkt der Schätzung messbar sind. Beispielsweise sind Attribute wie ''Length'', ''Transactions'' oder ''Entities'' aus dem Desharnais-Datensatz zu Beginn eines Projekts, wo die Schätzung im Regelfall erfolgt, noch gar nicht bekannt. Das Problem würde sich nur verlagern, würde man diese Messwerte im Endeffekt wieder manuell schätzen müssen.

\subsection{Ausblick} \label{future-work}

Im Laufe der Arbeit sind mehrere Veränderungen am Ablauf oder den Methoden aufgefallen, die für eine Verbesserung sorgen könnten.

Problematisch sind laut \fullref{results} bzw. \fullref{conclusion} die Präzision sowie die Zuverlässigkeit der Methoden.

Zuallererst sollten Model Tree-spezifische Split-Kriterien ausprobiert und evaluiert werden. Anbieten würde sich der M5P-Algorithmus aus \cite{quinlan1992learning,Wang97inducingmodel}. Möglich wäre es auch, die Stichproben nach Linearität aufzuteilen.

Eine andere, denkbare Ursache wäre zudem, dass die hier verwendeten Datensätze alle zu klein waren und ein größerer Datensatz Abhilfe schaffen würde sowohl in Sachen Präzision als auch Zuverlässigkeit. Ein solcher Datensatz wäre möglicherweise der kostenpflichtige ISBSG-Datensatz \cite{2015a}. Alternativ müsste solch ein Datensatz (mit im besten Fall aktuelleren Daten) gesammelt werden.

Mit mehr Rechenkraft und Zeit sollte überprüft werden, ob eine intensivere Zufallssuche, ein Algorithmus aus der Schwarmintelligenz oder gar eine Gridsuche bei der Wahl der besten Parameterkombination einen positiv Einfluss auf Präzision und/oder Zuverlässigkeit haben könnte.

Über Präzision und Zuverlässigkeit hinaus wären während der Ausarbeitung dieser Arbeit folgende Punkte hilfreich gewesen: Zum einen eine Analyse darüber, welche Methoden für welche Art von Datensätzen geeigneter sind als andere. Zum anderen empirisch ermittelte Werte, welche Präzision Expertenschätzungen in der Praxis erreichen. Mit diesen Werten wäre eine praktische Grenze gegeben, ab deren Nähe sich automatische Schätzverfahren lohnen würden.

Zudem bedarf die Korrelation zwischen Random Forest mit Model Trees zur Größe der Datensätze aus der \fullref{analysis} verstärkte Nachforschungen. 

\end{document}