\documentclass[../main.tex]{subfiles}
 
\begin{document}
 
\section{Methoden} \label{experiment}

In diesem Kapitel geht es um den in dieser Arbeit vorgenommenen Versuch. Angefangen bei dem groben \fullref{ablauf} geht es über das \fullref{exp-preprocessing} und der \fullref{hypertuning} der \fullref{methoden} hin zur konkreten Implementierung.

\subsection{Ablauf} \label{ablauf}

Der generelle Ablauf läuft pro genutztem Datensatz immer gleich ab. Vorbereitend vor jeglichen Berechnungen erfolgt ein Preprocessing-Schritt über dem gesamten Datensatz. Mit diesen vorbereiteten Daten kann dann die eigentliche Berechnung starten.

Die Daten werden zu Beginn mit einem zufälligen Split in eine Trainings- und Testmenge eingeteilt. Die Testmenge wird beiseite gelegt, während die Trainingsmenge zuerst benutzt wird, um eine Parameteroptimierung \bref{hypertuning} der Methode vorzunehmen. Dazu werden 100 zufällig ausgewählte Parameterkombinationen \tabref{tab:hypertuning-params} verglichen, um die beste Kombination zu ermitteln. Die ermittelte, beste Kombination wird benutzt, um erneut ein Modell zu trainieren, welches dann auf die verbliebene Testmenge angewendet wird und interpretierbare Ergebnisse erzeugt.

Dieser Durchgang vom Split über die Parameteroptimierung bis zum Erlernen und Evaluieren des Modells sollte mehrmals wiederholt werden \cite{Kirsopp2002}. Da weniger als fünf Wiederholungen zu unvertrauenswürdigen Ergebnissen führen, werden mehr als 20 Durchgänge \cite{Kirsopp2002} empfohlen. Zeitlich erwies sich eine Anzahl von 30 Durchgängen als praktikabel.

\begin{algorithm}[!htbp]
\DontPrintSemicolon
	\KwInput{dataset}
	dataset $\gets$ preprocess(dataset)\;
	\For{$i\gets1$ \KwTo $30$}{
		train, test $\gets$ split\_data(dataset, 0.2)\;
		\;
		params $\gets$ hypertune(DecisionTree(), train, 100)\;
		evaluate(DecisionTree(params), train, test)\;
		\;
		params $\gets$ hypertune(ModelTree(), train, 100)\;
		evaluate(ModelTree(params), train, test)\;
		\;
		params $\gets$ hypertune(RandomForestWithDecisionTrees(), train, 100)\;
		evaluate(RandomForestWithDecisionTrees(params), train, test)\;
		\;
		params $\gets$ hypertune(RandomForestWithModelTrees(), train, 100)\;
		evaluate(RandomForestWithModelTrees(params), train, test)\;
	}
\caption{Ablauf als Pseudocode}
\end{algorithm}

\subsection{Preprocessing der Datensätze} \label{exp-preprocessing}

In dieser Sektion soll nachvollziehbar beschrieben werden, inwiefern die Datensätze, die in \fullref{datasets} unverändert beschrieben wurden, in dieser Arbeit verändert und vorbereitet worden sind für den weiteren \fullref{ablauf}.

Wenn man sich \tabref{tab:before-after} anschaut, sieht man, dass während des Preprocessings lediglich ein Datensatz, der Desharnais-Datensatz, unvollständige Einträge enthielt, die entfernt worden waren.

Anders sieht das in Sachen Projektattribute aus. Abgesehen vom Albrecht-Datensatz ist überall ein Anstieg der Attributanzahl aufgrund der \fullref{bg-ohe} zu verzeichnen. Man sieht bei den Datensätzen Maxwell und CocomoNasa einen extremen Anstieg der Attribute, womit diese beiden Datensätze eher kategorischer Natur sind, wohingegen der moderate Anstieg (wenn überhaupt) der restlichen Datensätze darauf schließen lässt, dass eher numerische Kennzahlen gesammelt worden waren.

\begin{table*}[!htbp]
\centering
\begin{tabular}{l|rr|rr}
	\hline
	 & \multicolumn{2}{c|}{\thead{Vorher}} & \multicolumn{2}{c}{\thead{Nachher}} \\
	 & \thead{\# Proj.} & \thead{\# Attr.} & \thead{\# Proj.} & \thead{\# Attr.} \\
	\hline
	Desharnais & 81 & 12 & 77 & 13 \\
	Maxwell & 62 & 27 & 62 & 97 \\
	Albrecht & 24 & 8 & 24 & 8 \\
	CocomoNasa & 60 & 17 & 60 & 92 \\
	Kemerer & 15 & 8 & 15 & 14 \\
	Miyazaki94 & 48 & 9 & 48 & 8 \\
	\hline
\end{tabular}
\caption{Vergleich der Statistiken vor bzw. nach dem Preprocessing}
\label{tab:before-after}
\end{table*}

\subsubsection{Desharnais}

Es existieren im Desharnais-Datensatz 4 unvollständige Projekte \bref{bg-desharnais}. Diese wurden zuallererst entfernt.

Die Attribute ''Project'' und ''YearEnd'' wurden auch aussortiert, da sie inhaltlich weder zum Lernen des Models was beitragen noch zur Interpretation etwaiger Ergebnisse. Das Attribut ''Effort'' wurde vom Preprocessing als Zielvariable ausgenommen. Nur eines der verbliebenen Attribute ist nominal \tabref{tab:desh-attr}, ''Language'', welches zuerst mithilfe einer \fullref{bg-ohe} in die drei einzelne Attribute ''Is\_BasicCobol'', ''Is\_AdvancedCobol'' und ''Is\_4GL'' umwandeln. Diese 3 Attribute zusammen mit den restlichen 8 Attribute wurden noch z-normalisiert.

\subsubsection{Maxwell}

Im \fullref{bg-maxwell}-Datensatz wurden 18 kategoriale Attribute one-hot-kodiert (''T01'' bis ''T15'' sowie ''App'', ''Har'' und ''Dba''). Die Attribute ''Telonuse'', ''Ifc'' und ''Source'' sind zwar auch kategorial, jedoch bereits im binären Zustand \tabref{tab:maxw-attr}. Durch die hohe Anzahl kategorialer Attribute und der Kodierung ist die immens hohe Anzahl an Attributen für den Maxwell-Datensatz nach dem Preprocessing aus \tabref{tab:before-after} zu erklären.

Die binären Attribute sowie die verbleibenden vier nominalen Attribute (ausgenommen die Zielvariable ''Effort'') wurden abschließend ebenso z-normalisiert.

\subsubsection{Albrecht}

Im \fullref{bg-albrecht}-Datensatz gibt es keine kategorialen Attribute, weswegen lediglich alle Attribute bis auf die Zielvariable ''Effort'' z-normalisiert wurden.

\subsubsection{CocomoNasa} \label{prep-nas1}

Im \fullref{bg-nasa} befinden sich Zeichenketten im Großteil der Attribute. Genauer gesagt sind lediglich ''LOC'' und die Zielvariable ''ACT\_EFFORT'' nominal. Die Daten besitzen für restlichen 15 Attribute je einen der folgenden sechs Werte: ''Very\_Low'', ''Low'', ''Nominal'', ''High'', ''Very\_High'' oder ''Extra\_High''.

Da diese Werte eine Ordnung darstellen, werden sie mithilfe des \fullref{bg-le} durch ihre entsprechende Zahl von 1 - 6 ersetzt. Danach wurden die 15 Attribute one-hot-kodierte und die entstandenen Attribute zusammen mit ''LOC'' z-normalisiert.

\subsubsection{Kemerer}

Der \fullref{bg-kemerer}-Datensatz hat mit ''Language'' und ''Hardware'' zwei kategoriale Attribute, die one-hot-kodiert werden, ehe die entstandenen binären Attribute zusammen mit den verbleibenden vier nominalen Attributen z-normalisiert werden. Die Variable ''EffortMM'' bleibt unberührt.

\subsubsection{Miyazaki94}

Auch im \fullref{bg-miyazaki}-Datensatz gilt: Aufgrund der Nicht-Existenz von kategorialen Daten werden abgesehen von der Zielvariable ''MM'' alle Daten z-normalisiert.

\subsection{Konfiguration und Optimierung} \label{hypertuning}

Wie in \fullref{ablauf} dargestellt, werden die Konfigurationen der untersuchten Methoden Decision Tree, Model Tree, Random Forest mit Decision Trees sowie mit Model Trees über ein Optimierungsverfahren pro Methode und pro Split bestimmt. Dies wird gemacht, da nicht davon ausgegangen werden kann, dass es eine Parameterkombination gibt, die unter jedem zufällig bestimmten Split, geschweige denn für jede Methode, mit Bestimmtheit stets die bestmöglichsten Ergebnisse zu Tage bringt. Um überhaupt bestimmen zu können, welche Parameterkombination die Beste ist bzw. um Kombinationen vergleichen zu können, muss eine Scoring-Funktion festgelegt werden, die anhand der tatsächlichen und pro Kombination geschätzten Aufwände angibt, wie gut die Schätzungen sind. Dafür eignen sich die erwähnten \fullref{metriken}. Die Metrik, nach welcher optimiert werden soll, ist PRED(0,25), da hier extreme Ausreißer nicht so ins Gewicht fallen.

\begin{sloppypar}
In der verwendeten Implementierung der Methoden ist es für folgende Parameter sinnvoll, sie zu optimieren: ''max\_depth'', ''min\_samples\_split'', ''min\_samples\_leaf'', ''max\_features'', ''criterion'' und ''max\_leaf\_nodes''. Bei den Random Forest-Implementierungen kommt noch ''bootstrap'', ''n\_estimators'' und ''n\_jobs'' dazu, wobei letztere beiden auf 20 bzw. -1 (für parallele Berechnungen) fest gesetzt worden sind. ''n\_estimators'' ist nur im Kontext der Parameteroptimierung auf 20 gesetzt, da sonst die Berechnungszeit schnell in die Höhe geht. Beim Lernen des endgültigen Models werden 2000 Bäume berechnet. Die Erklärungen der einzelnen Parameter sind in \tabref{tab:hypertuning-params} zu finden.
\end{sloppypar}

Bei den restlichen Parameter sind vom Standard abweichende Werte als unwesentlich (z.B. ''min\_weight\_fraction\_leaf'') oder sogar falsch (z.B. ''random\_state'') eingeschätzt worden.

Die in \tabref{tab:hypertuning-params} angegebenen, möglichen Wertebereiche der Parameter wurden aufgrund eigener Vorüberlegungen und undokumentierten Versuchen vorausgewählt. Diese Entscheidungen sollen im Folgenden kurz erklärt werden.

Für ''max\_depth'' sollte der Wertebereich von 1 bis 20 ausreichen. Es wurde nie auch nur ansatzweise ein Baum mit einer Tiefe an die 20 erreicht. Zusätzlich ist None als Wert möglich, der den Baum gar nicht in der Tiefe beschränkt.

Bei ''min\_samples\_split'' wurden 5 mögliche Werte angegeben, die den Anteil aller Einträge angeben, die pro Split verfügbar sein müssen. Umso höher die Werte, desto weniger Splits werden tendenziell zugelassen. Daher wurden Werte über 0,3 ausgeschlossen.

Die Situation ist bei ''min\_samples\_leaf'' sehr ähnlich: je höher der Anteil an Einträgen pro Blatt sein muss, desto weniger Blätter sind potenziell möglich, weswegen Werte höher als 0,5 ausgeschlossen worden sind.

''max\_features'' deckt die ganze Spannbreite ab von maximal einem Attribut bis zur vollen Anzahl aller vorhandenen Attribute.

Bei ''criterion'' ist über die beiden Werte hinaus lediglich noch friedman\_mse möglich, welches aufgrund der Nähe zu mse ignoriert wurde.

Da ''max\_leaf\_nodes'' nur eine Maximalanzahl an Blättern vorgibt, eine geringere Anzahl jedoch immer möglich ist, wurden relativ große Schritte gemacht. Bäume mit mehr als 20 Blättern wurden nie generiert. Nichtsdestotrotz wäre dies dennoch mit None als Wert möglich.

''bootstrap'' gilt lediglich für Random Forest und deckt als binäre Option alle Möglichkeiten ab.

Die Werte für ''n\_estimators'' und ''n\_jobs'' wurden im Hinblick auf die Dauer der Parameteroptimierung festgelegt.

Die Optimierung erfolgt, indem 100-mal für alle Parameter ein Wert per Zufall aus dem angegebenen Wertebereich \tabref{tab:hypertuning-params} genommen wird. Diese Parameterkombination wird dann zum Trainieren und Evaluieren der Methoden verwendet, wobei die Leave-One-Out-Kreuzvalidierung zum Einsatz kommt. Die Kombination, die die besten PRED(0,25)-Ergebnisse generieren konnte, gilt für den Durchlauf als beste Parameterkombination.

\subsection{Implementierung}

Zur Ausführung aller beschriebenen Abläufe und Algorithmen wurde auf Jupyter Notebook 5.7.8 gesetzt, welches in einem Docker-Container auf Basis von Ubuntu 18.04 läuft. Als Kernel der Notebooks diente Python 3.6.7, zur Paketverwaltung wurde pip 19.0.3 verwendet. Zusätzlich wurde Graphviz 2.40.1 installiert.

10 verschiedene Python-Pakete kamen desweiteren zum Einsatz \tabref{tab:python-packages}.

Docker bekam Ressourcen in Höhe von 11,438GB Arbeitsspeicher und 3 Prozessoren.

\subsubsection{Decsion Tree und Random Forest}

\begin{sloppypar}
Bei dem Einsatz von Decision Tree und Random Forest wurde unverändert auf die fertigen Implementierungen von scikit-learn \bref{tab:python-packages} \textit{sklearn.tree.DecisionTreeRegressor} und \textit{sklearn.ensemble.RandomForestRegressor} gesetzt.
\end{sloppypar}

\subsubsection{Model Tree} \label{mt-impl}

Für Model Trees gab es zum Zeitpunkt der Bearbeitung keine Implementierung in einer bekannten oder halbwegs verbreiteten Bibliothek. Daher soll hier die vorgenommen Implementierung\footnote{\url{https://gitlab.com/reneburghardt/bachelorthesis-experiment/blob/master/libs/tree/ModelTree.py}} von Model Trees besprochen werden.

\begin{sloppypar}
Da für die Berechnungen im Allgemeinen scikit-learn zum Einsatz kam, wurde auf dessen \textit{sklearn.tree.DecisionTreeRegressor} aufgebaut. Darüber hinaus kam \textit{sklearn.linear\_model.LinearRegression} für die lineare Regression, \textit{sklearn.utils.validation.check\_is\_fitted} für die Validierung und \textit{numpy} für Vereinfachung im Arrayhandling zu Hilfe.
\end{sloppypar}

Die einzigen beiden wichtigen Methoden, die von \textit{DecisionTreeRegressor} (neben der \textit{\_\_init\_\_()}-Methode) überschrieben werden müssen, sind \textit{fit()} und \textit{predict()}. \textit{fit()} ist dafür zuständig, das Modell zu trainieren. Dafür notwendig als Parameter sind die Trainingsdaten \textit{X} und deren Zielwerte \textit{y}.

\begin{algorithm}[!htbp]
\DontPrintSemicolon
	\KwInput{X, y}
	tree $\gets$ DecisionTreeRegressor.fit(X, y) \label{alg:mt-fit:tree} \;
	\For{leaf \In tree.leafs}{
		leaf.model $\gets$ LinearRegression() \label{alg:mt-fit:save} \;
		leaf.model.fit(leaf.samples.X, leaf.samples.y) \label{alg:mt-fit:train} \;
	}
\caption{fit()-Methode der Model Trees}
\label{alg:mt-fit}
\end{algorithm}

Die Methode trainiert einen Decision Tree auf gegebenes X und y \algref{alg:mt-fit:tree} und geht dann die Blätter des entstandenen Baums durch um pro Blatt eine lineare Regression über die Samples des Blatts und deren Zielwerte zu trainieren \algref{alg:mt-fit:train}.

Die \textit{predict()}-Methode wird mit zu schätzenden Werten X aufgerufen und gibt die Ergebnisse des Modells zurück \algref{alg:mt-predict:return}. Dementsprechend muss auch diese Methode angepasst werden, damit die linearen Regressionsmodelle an den Blättern berücksichtigt werden.

\begin{algorithm}[!htbp]
\DontPrintSemicolon
	\KwInput{X}
	\If{not check\_is\_fitted()}{ \label{alg:mt-predict:check}
		\Raise NotFittedError \label{alg:mt-predict:raise} \;	
	}
	predictions $\gets$ [ ] \;
	\For{x \In X}{
		leaf $\gets$ tree.apply(x) \label{alg:mt-predict:apply} \;
		predictions.add(leaf.model.predict(x))  \label{alg:mt-predict:predict} \;
	}
	\Return predictions \label{alg:mt-predict:return} \;
\caption{predict()-Methode der Model Trees}
\label{alg:mt-predict}
\end{algorithm}

Die \textit{predict()}-Methode überprüft zuerst mit \textit{check\_is\_fitted()}, ob der Model Tree schon trainiert wurde \algref{alg:mt-predict:check} und wirft ansonsten einen Fehler \algref{alg:mt-predict:raise}. Wenn das Modell trainiert wurde, wird pro Wert aus X ermittelt, zu welchem Blatt der Wert gehört \algref{alg:mt-predict:apply} und dann der tatsächliche Zielwert von dem lineare Regressionsmodell des entsprechenden Blatts vorhergesagt \algref{alg:mt-predict:predict}.

\subsubsection{Random Forest mit Model Trees}

Da auch für Random Forest mit Model Trees keine Implementierung zu finden war und sich die \textit{sklearn.ensemble.RandomForestRegressor} aus scikit-learn nicht via Parameter auf andere Methoden umstellen ließ, musste die Methode auch codeseitig realisiert werden\footnote{\url{https://gitlab.com/reneburghardt/bachelorthesis-experiment/blob/master/ libs/forest/RandomForestModelTree.py}}.

Die obige Implementierung der Model Trees \bref{mt-impl} wird auch in der Implementierung des Random Forest mit Model Trees wiederverwendet: \textit{RandomForestRegressor} wird erweitert und die Model Trees \bref{mt-impl} als \textit{base\_estimator} in der \textit{\_\_init\_\_}-Methode angegeben.

\end{document}