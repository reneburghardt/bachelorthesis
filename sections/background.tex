\documentclass[../main.tex]{subfiles}
 
\begin{document}

\section{Grundlagen} \label{background}

\subsection{Datensätze} \label{datasets}

Da maschinelles Lernen datengetriebenes Berechnen bzw. Erstellen von Modellen ist, ist es wichtig, die zu Grunde liegenden Daten gut und genau zu kennen. Daher soll in dem folgenden Abschnitt die in dieser Arbeit verwendeten Datensätze besprochen, soweit bekannt, ihr Hintergrund angesprochen und Besonderheiten erwähnt werden. Alle hier genannten und eingesetzten Datensätze waren zum Zeitpunkt der Bearbeitung frei verfügbar.

Gemein ist allen Datensätzen, dass sie eine Liste von Softwareprojekten sind, welche über eine Menge an Attributen charakterisiert werden. Die Menge der Attribute ist zwischen den Datensätzen verschieden. Alle Datensätze lagen im ARFF (Attribute-Relation-File)-Format \cite{garner1995} vor.

\begin{table*}[!htbp]
\centering
\setlength\tabcolsep{3pt}
\begin{tabularx}{\textwidth}{X|s|s|s|s|s|s|s|s|r}
	\hline
	 &  &  & \multicolumn{7}{c}{\thead{Aufwand}} \\\cline{4-10}
	 & \SMALL \thead{\# \\ Proj.} & \SMALL \thead{\# \\ Attr.} & \SMALL \thead{Min.} & \SMALL \thead{Max.} & \SMALL \thead{Mean} & \SMALL \thead{Median} & \SMALL \thead{Sk.} & \SMALL \thead{Kurt.} & \SMALL \thead{Einheit} \\
	\hline
	Desharnais & 81 & 12 & 546,0 & 23940,0 & 5046,31 & 3647,0 & 2,01 & 4,72 & Stunden \\
	Maxwell & 62 & 27 & 583,0 & 63694,0 & 8223,21 & 5189,5 & 3,35 & 13,70 & Stunden \\
	Albrecht & 24 & 8 & 0,5 & 105,2 & 21,88 & 11,45 & 2,30 & 4,67 & 1,000 Stunden \\
	CocomoNasa & 60 & 17 & 8,4 & 3240,0 & 406,41 & 118,8 & 2,68 & 7,33 & Monate \\
	Kemerer & 15 & 8 & 23,2 & 1107,31 & 219,25 & 130,3 & 3,07 & 10,59 & Monate \\
	Miyazaki94 & 48 & 9 & 5,6 & 1586,0 & 87,47 & 38,1 & 6,26 & 41,36 & Monate \\
	\hline
\end{tabularx}
\caption{Statistiken der genutzten Datensätze}
\label{tab:data-stats}
\end{table*}

In \tabref{tab:data-stats} sieht man allgemeine Statistiken. Die aufgelisteten Datensätze variieren stark: Kemerer hat mit 15 Projekten die geringste Größe, wohingegen Desharnais mit 81 Projekten beinahe das sechsfache davon umfasst.

Bei den Attributen besitzt Kemerer wieder, zusammen mit Albrecht, mit je 8 Attributen die geringste Anzahl. Dahingegen wurden in Maxwell 24 Attribute gesammelt.

\begin{figure}[!htbp]
\centering
\includegraphics[width=15cm]{../imgs/efforts}
\caption{Boxplots der Aufwände in Stunden (links) bzw. in Monaten (rechts) \\ (Die Aufwände des Albrecht-Datensatzes wurden mit 1000 multipliziert.)}
\label{fig:efforts}
\end{figure}

Minimum (Min.), Maximum (Max.), Mittelwert (Mean) und Median der Aufwände sind unter den Datensätzen aufgrund der verschiedenen Einheiten schwer zu vergleichen. Daher wurden in \figref{fig:efforts} die Aufwände der Datensätze nach Einheit getrennt visualisiert.

Die Datensätze Desharnais, Maxwell und Albrecht besitzen alle ein minimales Projekt von ähnlicher Größe (\textasciitilde 500h), jedoch steigt die Spanne bis zum maximalen Projekt von Desharnais über Maxwell bis zu Albrecht immer weiter an. Alle drei besitzen Ausreißer nach oben, jedoch keine nach unten. Das heißt, dass die Datensätze eher aus kleineren Projekten besteht. 

CocomoNasa, Kemerer und Miyazaki zeigen unterschiedliche Ausprägungen: CocomoNasa hat die größe Spanne an Projektumfängen mit vielen Ausreißern nach oben. Kemerer hat die geringste Spanne der drei Datensätze und nur einen Ausreißer nach oben. Der Großteil der Projekte aus Miyazaki94 besitzen einen eher kleineren Aufwand. Dennoch gibt es einige Ausreißer nach oben.

In jedem Datensatz weicht der Mittelwert stark vom Median ab \tabref{tab:data-stats}. Dies steht auch im Einklang damit, dass jeder Datensatz eine recht hohe, linkssteile Skewness (Sk.) hat. Laut \cite{curran1996robustness} sind Daten mit einem absoluten Wert der Skewness kleiner als 2 und einem absoluten Wert der Kurtosis (Kurt.) kleiner als 7 normalverteilt. Dies trifft auf keinen hier behandelten Datensatz zu und bringt laut \cite{Azzeh2015} eine Herausforderung beim Entwickeln akkurater Modelle mit sich. Die Skewness und Kurtosis der Datensätze und ihre Auswirkungen werden in der \fullref{analysis} noch behandelt.

\subsubsection{Desharnais} \label{bg-desharnais}

Der Desharnais-Datensatz aus dem PROMISE-Repository \cite{Sayyad-Shirabad+Menzies:2005} ist ein Datensatz mit Daten von Softwareprojekten eines kanadischen Unternehmens aus den 1980er-Jahren \cite{desharnais1988statistical}. Trotz des relativ hohen Alters der Daten wird der Datensatz immer noch in empirischen Studien eingesetzt (\cite{Mair2000,Azzeh2011,Zakrani2018}).

Der Datensatz besteht aus insgesamt 81 Projekten, wobei 4 dieser Projekte nicht vollständig sind. Die vollständigen Projekte besitzen je 12 Attribute \tabref{tab:desh-attr} wovon alle numerischer Natur sind außer ''Languages'', welches eine von drei Kategorien darstellt \tabref{tab:desh-attr}. Das Attribut ''Effort'' gilt als Zielvariable.

\subsubsection{Maxwell} \label{bg-maxwell}

Ähnlich oft eingesetzt bei Arbeiten zum Thema Aufwandsschätzungen von Softwareprojekten \cite{Sentas2005,Radlinski10onpredicting,Azzeh2011}, ist der Maxwell-Datensatz \cite{yanfu_li_2009_268461}, der aus etwas jüngeren Daten besteht. Er wurde als Anschauungsbeispiel für lineare Regressionsmodelle genutzt \cite{Sentas2005} und beinhaltet 62 Projekte einer der größten finnischen Banken aus den Jahren von 1985 bis 1993. Jedes dieser Projekte ist beschrieben durch 27 Attribute. Aufgelistet sind diese Attribute in \tabref{tab:maxw-attr}. Die Zielvariable des Datensatzes ist ''Effort''.

Auch wenn alle Werte im Datensatz numerisch sind, stellt der Großteil kategoriale Attribute dar. Prominent dafür stehen die Attribute T01 - T15, welche je einen Aspekt des Projekts bezüglich der Arbeit und dem Vorgehen auf einer Skala von 1 bis 5 kategorisieren und damit auch eine Reihenfolge abbilden. Außergewöhnlich ist aber, dass die Kategorisierungen fast formalen Definitionen folgen \cite[Tabelle~5]{zelkowitz2003advances}.

Neben diesen Attributen gibt es mit ''App'', ''Har'', ''Dba'', ''Ifc'', ''Source'' und ''Telonuse'' noch weitere kategoriale Attribute, die jedoch keiner Ordnung unterliegen. Somit bleiben neben der Zielvariable noch fünf weitere, metrische Attribute: ''Syear'', ''Nlan'', ''Duration'', ''Size'' und ''Time''.

Im Datensatz gibt es keine fehlenden Werte. Im Original gab es dennoch 63 statt 62 Projekte. Das eine Projekt wurde jedoch als Außenseiter erkannt und entfernt \cite{Sentas2005}. Im verwendeten Datensatz \cite{yanfu_li_2009_268461} ist dieser Außenseiter gar nicht enthalten.

\subsubsection{Albrecht} \label{bg-albrecht}

Der Albrecht-Datensatz \cite{li_yanfy_keung_jacky_w_2010_268467} ist ein Datensatz an Projekten, die in den 1970er-Jahren bei IBM fertiggestellt worden sind. Insgesamt besteht der Datensatz lediglich aus 24 Projekten, wovon 18 in COBOL geschrieben worden sind, 4 in PL1 und 2 in einer unspezifizierten DBMS-Sprache. Alle Projekte sind vollständig.

Die im Datensatz gesammelten Attribute entsprechen denen, die für Albrechts Function Points-Methode zum Einsatz kommen und sind in \tabref{tab:albr-attr} erläutert. Sie sind alle metrisch. Die Zielvariable ''Effort'' ist gemessen in 1.000-Personenstunden.

\subsubsection{CocomoNasa} \label{bg-nasa}

Der CocomoNasa-Datensatz ist ein im PROMISE-Repository \cite{Sayyad-Shirabad+Menzies:2005} verfügbarer Datensatz aus dem Umfeld der NASA. Er beinhaltet 60 Projekte aus den 1980er- und 1990er-Jahren von verschiedenen Center der bekannten US-Bundesbehörde.

Besonders an diesem Datensatz ist der sehr hohe Anteil kategorialer Attribute. Insgesamt 15 Stück sind davon vorzufinden \tabref{tab:nasa-attr}. Lediglich ein Attribut ''LOC'' und die Zielvariable ''ACT\_EFFORT'' sind nominal.

\subsubsection{Kemerer} \label{bg-kemerer}

Die Daten des Kemerer-Datensatzes \cite{jacky_w_keung_2010_268464} bestehen aus 15 Projekten eines Beratungs- und Dienstleistungsunternehmens, welches sich auf datenverarbeitende Software spezialisierte, die überwiegend in Cobol geschrieben worden sind. Die Projekte wurden alle zwischen 1981 und 1985 bearbeitet.

Die Zielvariable ist ''EffortMM'' und in Personenmonaten berechnet. Die Werte dürften aufgrund der Tatsache, dass sie von einer Beratungsfirma erhoben worden sind, die von ihren Kunden nach Stunden bezahlt wird, ziemlich genau sein. Abgesehen von der Zielvariable sind im Kemerer-Datensatz noch zwei kategoriale Attribute (''Language'' und ''Hardware'') zu finden sowie vier numerische Attribute (''Duration'', ''KSLOC'', ''AdjFP'' und ''RAWFP'') \tabref{tab:keme-attr}.

\subsubsection{Miyazaki94} \label{bg-miyazaki}

Unterschiedlich zu den meisten anderen Datensätzen besteht der Miyazaki94-Datensatz \cite{sousuke_amasaki_2016_268473} aus Projekten, insgesamt 48, von 20 unterschiedlichen Softwareunternehmen einer übergeordneten Fujitsu-Gruppe.

De Zielvariable ist ''MM'', der Aufwand in Personenmonaten, und beinhaltet auch die Zeiten, die nötig waren für Systementwurf und -test sowie Projektmanagement. Der Datensatz beinhaltet keine kategorialen Attribute \tabref{tab:miya-attr}.

\subsection{Preprocessing}

Das Vorverarbeiten von Daten hat einen signifikanten Einfluss auf die Vorhersagen der Methoden des maschinellen Lernens \cite{Huang2015}. Aus diesem Grund sollen im Folgenden die angewandten Preprocessing-Schritte erläutert werden.

\subsubsection{Label-Encoding} \label{bg-le}

Einige der in \bref{datasets} vorgestellten Datensätze besitzen kategoriale Attribute, deren Werte Zeichenketten bilden. Beispielsweise in CocomoNasa werden alle Attribute, die Kategorien repräsentieren, mit Werten von ''Very\_Low'' bis ''Extra\_High'' belegt. Methoden des maschinellen Lernens können mit dieser Form von Daten nicht gut umgehen. Daher müssen diese Attribute vorher noch in eine numerische Darstellung transformiert werden. 

Mithilfe eines Label Encoders kann diese Transformation durchgeführt werden. Der Encoder nimmt ein Attribut und ersetzt jede eindeutige Zeichenkette durch eine eindeutige Zahl. Dies ist eine 1-zu-1-Zuordnung der Zeichenketten zu numerischen Werten. Je nach Attribut und dessen Bedeutung muss beim Encoden aufgepasst werden, dass die Ordnung der Werte eingehalten wird. So ist für das oben erwähnte Beispiel ratsam, ''Very\_Low'' durch 1 zu ersetzen und ''Extra\_High'' durch 6 (die Kategorien dazwischen respektive 2 - 5).

\subsubsection{One-Hot-Kodierung} \label{bg-ohe}

Viele der in dieser Arbeit genutzten Datensätze besitzen kategorische Attribute in numerischer Form, entweder direkt  oder nach dem \fullref{bg-le}. Anders als bei der Repräsentation von Kategorien durch Zeichenketten, ist es für die Methoden möglich mit numerischen Attributen zu lernen. Dennoch kann dies zu Problem führen, da die Methoden die Semantik der Attribute nicht kennen und die Daten missverstehen werden. Immer wenn Methoden mathematische Berechnungen oder Vergleiche benutzen, greifen sie auf die natürliche Ordnung der Zahlen in der Form 0 < 1 < 2 zurück. Diese Ordnung entspricht aber bei kategorischen Attributen nicht der Wahrheit. Um dieses Problem zu lösen, werden kategorische Attribute one-hot-kodiert.

Bei der One-Hot-Kodierung wird ein kategorisches Attribut in mehrere gespalten. Pro möglichem Wert $c$ eines Attribut entsteht ein neues Attribut, welches für einen Eintrag $x$ den Wert $1$ annimmt, sofern dieser Eintrag $x$ zu dem Wert $c$ gehört. Diese neuen Attribute haben somit dann einen binären Wertebereich.

\subsubsection{Z-Normalisierung} \label{bg-z}

Datensätze haben oft Attribute mit unterschiedlichen Wertebereichen. Diese Unterschiede können teilweise extrem groß ausfallen. Beispielsweise könnte ein Attribut das Alter von Personen sein, welches normalerweise in einen Bereich von 0 bis 100 fällt. Zusätzlich könnte die Größe der Person in Metern gesammelt worden sein. Der Bereich dazu liegt üblicherweise zwischen 0,5 und 2.

Attribute mit unterschiedlichen Wertebereichen können einen ungewollten Effekt auf die Güte der Modelle des maschinellen Lernens haben. Oft wird beispielsweise die Distanz zwischen Daten aufgrund ihrer Attribute berechnet. In solchen Berechnungen hätte eine Differenz von 1 im Alter einen viel geringeren Einfluss als eine Differenz von 1 in der Größe, auch wenn dies inhaltlich nicht stimmen würde. Um diese ungewollten Effekte zu beseitigen, werden die Daten normalisiert. Dabei werden die Wertebereiche der Attribute untereinander vereinheitlicht, die relativen Differenzen in den einzelnen Werten jedoch beibehalten.

Bei der Standardisierung werden die Werte so skaliert, dass der Mittelwert $\bar{x}$ 0 und die Varianz $\sigma$ 1 beträgt. Um das zu erreichen, wird jeder Wert durch seinen Z-Score ersetzt. Der Z-Score des Wertes x wird wie folgt berechnet $$z = \frac{x - \bar{x}}{\sigma}$$.

Auch wenn tree-basierende Methoden im Allgemeinen als unabhängig von verschiedenen Wertebereichen gelten, wird die Z-Normalisierung in dieser Arbeit auf jeden Datensatz angewandt. Dies liegt zum einen daran, da es in keinem Fall einen negativen Effekt hat und zum anderen daran, da bei den Model Tree-basierenden Methoden auch lineare Regression angewendet wird, die von negativen Effekten betroffen wäre.

\subsection{Methoden} \label{methoden}

In diesem Abschnitt soll die Funktionsweise der verwendeten Methoden des maschinellen Lernens vorgestellt und ihre Vor- und Nachteile besprochen werden. Neben den vier Methoden, die in dieser Arbeit verglichen werden sollen \bref{introduction}, wird auch die Lineare Regression vorgestellt, da sie ein fundamentaler Bestandteil von Model Trees ist.

\subsubsection{Lineare Regression}

Lineare Regression ist ein Analyseverfahren um die Beziehung zwischen einer abhängigen Variable und einer Menge an unabhängigen Variablen als lineare Funktion zu modellieren und somit möglicherweise Prognosen über die Zielvariable stellen zu können. In der einfachen linearen Regression, in der nur die Beziehung zwischen einer unabhängigen Variable $x$ zur abhängigen Variable $y$ untersucht wird, ist die Gleichung $y = \beta_1x_1 + \beta_0$ Grundlage zur Modellierung. Bei der multiplen linearen Regression werden mehrere unabhängige Variablen $x_1 ... x_n$ berücksichtigt: $y = \beta_nx_n + ... \beta_nx_n + \beta_0$.

\begin{figure}[!htbp]
\centering
\includegraphics[width=10cm]{../imgs/slr}
\caption{Beispiel einer einfachen linearen Regression}
\label{fig:slr}
\end{figure}

Um die Parameter $\beta_1 ... \beta_n$ zu finden, die das beste Modell für die Beziehung bilden, wird üblicherweise ein Suchverfahren angewandt, welches die durchschnittliche, geeignet berechnete Abweichung der Schätzungen zu historischen Realwerten (lokal) minimiert.

Nachteil von linearen Regressionsmodellen ist, dass sie bei nicht-linearen Daten sehr schlechte Ergebnisse liefern. Sofern aber lineare Daten vorliegen, entstehen bei der schnell und einfachen Berechnung gute Werte.

\subsubsection{Decision Tree} \label{bg-dt}

Ein Decision Tree (DT) ist ein Baum, dessen Knoten Entscheidungen darstellen, um die Klassifikation einer Eingabe vorzunehmen. Dazu geht man, vom Wurzelknoten aus beginnend, über die inneren Knoten bis zu einem Blatt des Baums, welches eine Klasse repräsentiert. Im Gegensatz zu den Blättern stellen die inneren Knoten Entscheidungen dar, die man anhand der Eingabeattribute beantwortet.

\begin{figure}[!htbp]
\centering
\includegraphics[width=12.5675676cm]{../imgs/dt}
\caption{Decision Trees. Links: Klassifikation. Rechts: Regression}
\label{fig:dt}
\end{figure}

DT haben zwei Stärken: 1. DT stellen Entscheidungen und die Entscheidungsfindung in einer für Menschen leicht verständlichen Form dar. So können selbst Laien einfach vorteilhafte Vorhersagen treffen, die auf der Erfahrung von Experten basieren. Die 2. Stärke ist die simple Berechnung von Entscheidungen anhand DT.

Die Variabilität in der Komplexität eines DT ist Vor- und Nachteil zugleich. Zum einen kann so die Komplexität an Punkte wie Tragweite der Entscheidung, Wissensstand der Anwender, etc. angepasst werden, andererseits ist es aber auch eine Herausforderung, die richtige Komplexität eines DT zu bestimmen. Während von Menschen erstellte DT oftmals dazu neigen, Probleme zu sehr herunterzubrechen, tendieren DT, die mithilfe maschinellem Lernen erstellt werden, eher zu Entscheidungsbäumen, die zu sehr auf die Trainingsdaten spezialisiert sind. Diese Problematik kann jedoch durch Restriktionen, wie einer maximalen Tiefe des Baums, oder Pruning angegangen werden, also dem Kürzen und Vereinfachen eines Baumes.

DT können auch auf Regressionsprobleme \figref{fig:dt} angewandt werden, indem die möglichen Klassen an den Blättern des Baums reelle Zahlen darstellen.

\subsubsection{Model Tree} \label{bg-mt}

Eine andere Art und Weise, Regressionsprobleme mit Decision Trees anzugehen, sind Model Trees (MT). MT sind spezielle DT, die an ihren Blätter nicht eine feste Zahl als Klasse aller dazugehörigen Eingaben haben, sondern eine lineare Funktion. Demnach gelten MT als Ensembles.

\begin{figure}[!htbp]
\centering
\includegraphics[width=15cm]{../imgs/dt-vs-mt}
\caption{Unterschied zwischen DT (links) und MT (rechts)}
\label{fig:dt-vs-mt}
\end{figure}

Beim Trainieren eines MT entsteht nicht nur ein DT als Modell, sondern an jedem Blatt des DT nochmal ein lineares Modell via Regressionsanalyse. Die Trainingsdaten werden dabei zuerst beim Trainieren des Baums klassifiziert, jede Eingabe aus der Trainingsmenge wird also einem Blatt zugeordnet, und diese Menge pro Blatt wird benutzt, um jeweils nochmal ein Modell mithilfe linearer Regression zu erlernen.

Die Vorteile gegenüber linearer Regression sind klar: Zum einen können Klassenattribute ohne Umwandlung in numerische Werte in die Erstellung des Modells fließen, zum anderen entgeht man dem Problem des Underfittings durch vorheriges Klassifizieren der Eingaben, wodurch die linearen Modelle an den Blättern die Daten viel präziser abbilden können. 

Gegenüber reinen DT besitzen MT einen wirklich kontinuierlichen Ergebnisraum anstatt den Durchschnitt einer klassifizerten Menge als numerischen Wert und damit pseudo-kontinuierlich anzugeben \figref{fig:dt-vs-mt}.

\subsubsection{Random Forest}

Random Forest (RF) wurde zuerst 2001 von Breiman in \cite{Breiman2001} vorgestellt und ist eine Methode des maschinellen Lernens. RF ist dabei ein Ensemble, also eine Methode, die eine endliche Anzahl anderer Methoden kombiniert, um unpräzise und/oder unzuverlässige Ergebnisse zu einem (hoffentlich) präziseren und zuverlässigeren Ergebnis zu bündeln. RF können dabei sowohl Klassifikations- als auch Regressionsprobleme lösen, je nachdem auf welchen Methoden sie aufbauen. Wenn auf z.B. klassische Decision Trees zur Klassifikation gebaut wird, wird üblicherweise die am meisten von den Bäumen vorhergesagte Klasse als Gesamtvorhersage angegeben. Bei der Lösung eines Regressionsproblem wird normalerweise der Durchschnitt der gesammelten Ergebnisse der Bäume gebildet und als Lösung angegeben.

\begin{figure}[!htbp]
\centering
\includegraphics[width=15cm]{../imgs/rf}
\caption{Visuelle Andeutung eines Random Forest}
\label{fig:rf}
\end{figure}

Beim Erstellen der Bäume eines RF existieren mehrere Variante: Entweder jeder Baum greift auf die gleiche, initiale Menge an Daten zu, auf eine zufällig bestimmte Untermenge oder alternativ auf Bootstrapping. Beim Bootstrapping wird normalerweise eine Menge der gleichen Größe wie die initiale Menge gezogen, jedoch werden gezogene Daten auch wieder zurückgelegt und für die weiteren Ziehungen verfügbar gemacht. Die Bäume werden in allen Varianten unabhängig voneinander und (sofern möglich) parallel erstellt. Dadurch haben RF den Vorteil, sehr schnell zu sein.

\subsection{Metriken} \label{metriken}

Um die verschiedenen Methoden evaluieren und ihre Präzision bestimmen zu können, werden zumeist folgende Metriken eingesetzt: MMRE, MdMRE und PRED(x). Diese drei Metriken finden in dem Großteil der Arbeiten über Aufwandsschätzungen von Softwareprojekten Verwendung \cite{Wen2012}. 

Diese Metriken bauen auf dem relativen Fehler (MRE für engl.: Magnitude of Relative Error) pro Projekt auf, welcher die Differenz zwischen tatsächlichen und geschätzten Aufwand relativ zum tatsächlichen Aufwand angibt. Mathematisch wird MRE wie folgt beschrieben: $$ MRE = \left|\left(\frac{\textrm{Effort}_{actual} - \textrm{Effort}_{predicted}}{\textrm{Effort}_{actual}}\right)\right| $$

Um eine Menge von $N$ Projekten inklusive dazugehöriger Schätzungen und damit ein Model zur Aufwandsschätzung von Softwareprojekten evaluieren zu können, wird u.a. üblicherweise der Durchschnitt der MRE-Werte aller Projekte (MMRE für engl.: Mean Magnitude of Relative Error) verwendet: $$ MMRE = \frac{1}{N} \sum_{i=1}^{N}MRE_i $$

Im Allgemeinen liegt ein akzeptabler Wert für MMRE bei nicht mehr als 25\% \cite{Zakrani2018}.

MMRE ist anfällig für Ausreißer, weswegen auch der Median der MRE-Werte aller Projekte (MdMRE für engl.: Median Magnitude of Relative Error) eingesetzt wird: $$ MdMRE = median(MRE_i) : i \in \{1 ... N\}$$

Da die Unsicherheit über die Schätzungen geringer wird, je kleiner die relativen Fehler pro Projekte sind, verhalten sich die Werte der genannten Metriken indirekt proportional zu der Präzision der Schätzungen \cite{nassif2014}.

Die Metrik PRED$(x)$ (engl.: prediction accuracy) gibt den Anteil der Projekte an, deren MRE-Wert kleiner oder gleich dem Wert $x$ ist. Mit anderen Worten gibt PRED$(x)$ den Anteil der Projekte an, deren Abschätzung die angestrebte Präzision erreicht. Anders als bei den obigen Metriken verhält sich die Präzision direkt proportional zu dem Wert des PRED($x$) \cite{nassif2014}. $$ PRED(x) = \frac{1}{N} \sum_{i=1}^{N}\left\{
	\begin{array}{lr}
        1 & MRE_i \leq x \\
        0 & sonst
    \end{array}
\right. $$

Häufig wird ein x mit dem Wert 0,25 oder 0,30 gewählt \cite{Baskeles2007}. Ein akzeptabler Wert für PRED($x$) liegt bei mindestens 75\% \cite{Baskeles2007}.

Ein Vorteil von PRED($x$) und MdMRE gegenüber MMRE ist, dass die beiden Metriken weniger anfällig auf Ausreißer sind. Zudem ist die Aussage von PRED($x$) am leichtesten zu interpretieren.

Auch wenn die drei bisher genannten Metriken die häufigsten sind, sind sie laut \cite{Foss2003} nicht skala-unabhängig und bevorzugen somit unter ansonsten gleichen Umständen kleinere Projekte über größere. Weitere Kritik ist in \cite{Miyazaki1991,Kitchenham2001,Sarro2016} zu finden. Aufgrunddessen wurden von mehreren Publikationen Alternativen vorgeschlagen, wovon einige vorgestellt und in dieser Arbeit angewendet werden sollen.

Zwei Vorschläge sind die balancierten, relativen Fehler MBRE (engl.: Mean Balanced Relative Error) und MIBRE (engl.: Mean Inverted Balanced Relative Error) \cite{Miyazaki1991}. MBRE und MIBRE liegen folgende Rechnungen zugrunde: $$ MBRE = \frac{1}{N} \sum_{i=1}^{N}\frac{\left|\textrm{Actual}_{i} - \textrm{Predicted}_{i}\right|}{min\left(\textrm{Actual}_{i}, \textrm{Predicted}_{i}\right)} $$

$$ MIBRE = \frac{1}{N} \sum_{i=1}^{N}\frac{\left|\textrm{Actual}_{i} - \textrm{Predicted}_{i}\right|}{max\left(\textrm{Actual}_{i}, \textrm{Predicted}_{i}\right)} $$

Zuletzt wurde die Evaluierung über den absoluten Fehler (MAE für engl.: Mean Absolute Error) vorgeschlagen: $$ MAE = \frac{1}{N} \sum_{i=1}^{N}\left|\textrm{Actual}_{i} - \textrm{Predicted}_{i}\right| $$

Nachteil an dieser Metrik ist, dass ein Vergleich datensatzübergreifend nicht mehr möglich ist und nur noch Vergleiche über Methoden an den gleichen Datensätzen gelingen. Deswegen kann auch kein allgemein akzeptabler Wert für diese Metrik genannt werden.

Trotz der Menge an Alternativen, gibt es keine übergreifende Einigung, welche Metrik am Besten geeignet ist \cite{Dejaeger2012}. Daher werden in der \fullref{analysis} alle vorgestellten Metriken angegeben. Dennoch musste für die Parameteroptimierung \bref{hypertuning}, die \fullref{analysis} und das \fullref{conclusion} eine vorrangige Metrik gewählt werden. Die Wahl fiel dabei aufgrund des häufigen Einsatzes in der Forschung und den oben genannten Vorteilen auf PRED(0,25).

\end{document}